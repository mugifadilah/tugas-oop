<?php 
require ('animal.php');
require ('frog.php');
require ('ape.php');


$sheep = new Animal("shaun");
echo "Name : ".$sheep->name."<br>"; // "shaun"
echo "Leg  : ".$sheep->legs."<br>"; // 2
echo "Cold Blooded  : ".$sheep->cold_blooded."<br><br>" ; // false


$sungokong = new Ape("kera sakti");
echo "Name : ".$sungokong->name."<br>"; // ""kera sakti"
echo "Leg  : ".$sungokong->legs."<br>"; // 2
echo "Cold Blooded  : ".$sungokong->cold_blooded ."<br>"; // false
echo "Yell : ";
echo $sungokong->yell()."<br><br>" ;// "Auooo"


$kodok = new Frog("buduk");
echo "Name : ".$kodok->name."<br>"; // "buduk"
echo "Leg  : ".$kodok->legs."<br>"; // 4
echo "Cold Blooded : ".$kodok->cold_blooded."<br>" ; // false
echo "Jump : ";
echo $kodok->jump() ."<br>"; // "hop hop"
?>
